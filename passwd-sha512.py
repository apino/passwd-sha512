#!/usr/bin/python3

__author__ = 'Alejandro Pino Oreamuno'

import argparse
import getpass
import sys
import crypt

def arg_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument("salt", help="the salt", type=str)
    parser.add_argument("-v", "--verbose", help="increase verbosity", action="store_true")
    return parser.parse_args()

def str_to_sha512(passwd, salt):
    print(crypt.crypt(passwd, "$6$"+salt))

def get_pass():
    password = getpass.getpass()
    if not password:
        raise Exception("Invalid password")
    return password

def main():
    args = arg_parse()
    try:
        str_to_sha512(get_pass(), args.salt)
        sys.exit(0)
    except Exception as err:
        print(err)
        sys.exit(1)

if __name__ == '__main__':
    main()
